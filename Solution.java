import java.util.Arrays;

/**
 * Rotate an N x N array 90 degrees in place
 *
 * Example: 1 2 3 
 *          4 5 6 
 *          7 8 9 
 *          ->
 *          7 4 1 
 *          8 5 2 
 *          9 6 3
 * 
 * Rotating the array means rotating outside in. This means rotating the outside
 * numbers first, then the inside numbers.
 * 
 */

public class Solution {

    public void rotateMatrix(int[][] matrix) {
        // layer is the minium row or column you can address
        // it starts at 0 so the minimum you can address is 0,0
        for (int layer = 0; layer < matrix.length/2; layer++) {
            int last = matrix.length - 1 - layer; // last is either the last row or the last column
            for (int i = layer; i < last; i++) {
            /**
             * we may have to adjust the column or row to based on which layer we are rotating
             * in tandem with last this adjusts the bounds on the layer we are rotating
             * (e.g. [5][5] at 0, the offset is 0 so the maximum you can address is 0,4 or 4,0. )
             * (e.g. [5][5] at 1, the offset is 1 so the maximum you can address is 1,3 or 3,1. )
             * the offset moves into the layer. we have to start on the first value (layer) and move to the final value (last)
             */
                int offset = i - layer; // 
                int tl = matrix[layer][i]; // we always start at the top left

                // BL -> TL or TL = BL
                matrix[layer][i] = matrix[last - offset][layer];
                // BR -> BL or BL = BR
                matrix[last - offset][layer] = matrix[last][last-offset];
                // TR -> BR or BR = TR
                matrix[last][last-offset] = matrix[i][last];
                // TL -> TR or TR = TL
                matrix[i][last] = tl;
            }
        }
    }

    public static void main(String[] args) {

        int[][] matrix = { 
            { 1, 2, 3 }, 
            { 4, 5, 6 }, 
            { 7, 8, 9 }, };

        int[][] expectedMatrix = { 
            { 7, 4, 1 }, 
            { 8, 5, 2 }, 
            { 9, 6, 3 }, };

        Solution s = new Solution();

        s.rotateMatrix(matrix);

        System.out.println(Arrays.deepToString(matrix));

        System.out.println(Arrays.deepEquals(matrix, expectedMatrix));
    }
}